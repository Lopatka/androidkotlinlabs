package com.example.labs.lab1.model.card


class DataSourceCard {
    private var source: ArrayList<Card> = ArrayList()

    init {
        source.add(Card("Dog title", "dog desc 1321 23213 312321 312313", Icon.DOG))
        source.add(Card("Cat title", "cat desc 3123213 3123 3123 12312312", Icon.CAT))
        source.add(Card("Fish title", "fish desc 3123 321451232 312321321312", Icon.FISH))
        source.add(Card("Rabbit title", "rabbit desc 1423421 321 4123 312312", Icon.RABBIT))
    }

    fun fetchData(): ArrayList<Card> {
        return source
    }
}