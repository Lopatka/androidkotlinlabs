package com.example.labs.lab3

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import com.example.labs.R
import com.example.labs.databinding.ActivityProductBinding
import com.example.labs.lab3.model.product.Product
import com.example.labs.lab3.model.product.ProductAdapter
import com.example.labs.lab3.repository.ProductRepository
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.reflect.Type


const val APP_LAB3_PREFERENCES = "APP_LAB3_PREFS"
const val PREFS_CART_JSON = "PREFS_CART_JSON"

class ProductActivity : AppCompatActivity() {
    private lateinit var binding: ActivityProductBinding
    private val productRepository = ProductRepository()
    private lateinit var arrayAdapter: ArrayAdapter<*>
    lateinit var preferences: SharedPreferences
    var products: MutableSet<Product> = mutableSetOf()

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProductBinding.inflate(layoutInflater)
        setContentView(binding.root)
        getAllProducts()
        preferences = getSharedPreferences(APP_LAB3_PREFERENCES, Context.MODE_PRIVATE)
        products = getProductFromPrefs()
        binding.textViewProductsCount.text = "Товаров в корзине: " + products.size.toString()
        println(products)
    }

    private fun getAllProducts() {
        var products: List<Product>
        CoroutineScope(Dispatchers.IO).launch {
            products = productRepository.getProducts()
            runOnUiThread {
                arrayAdapter = ProductAdapter(
                    this@ProductActivity,
                    R.layout.product_card,
                    products
                )
                binding.listViewProducts.adapter = arrayAdapter
            }
        }
    }

    fun getProductFromPrefs(): MutableSet<Product> {
        var products: MutableSet<Product> = mutableSetOf()
        val json: String? = preferences.getString(PREFS_CART_JSON, null)
        if (json != null) {
            val gson = Gson()
            val type: Type = object : TypeToken<MutableSet<Product?>?>() {}.type
            products = gson.fromJson(json, type)

            if (products == null) {
                products = mutableSetOf()
            }
        }
        return products
    }

    fun onClickGoToOrder(view: View) {
        val intent = Intent(this, OrderActivity::class.java)
        startActivity(intent)
    }
}