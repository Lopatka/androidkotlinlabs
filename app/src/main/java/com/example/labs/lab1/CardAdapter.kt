package com.example.labs.lab1

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import com.example.labs.R
import com.example.labs.lab1.model.card.Card

class CardAdapter(
    private val getContext: Context,
    private val layoutId: Int,
    private val listCard: ArrayList<Card>
) : ArrayAdapter<Card>(getContext, layoutId, listCard) {

    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val inflater = (getContext as Activity).layoutInflater
        val itemView: View = inflater.inflate(layoutId, parent, false)
        val icon: ImageView = itemView.findViewById(R.id.card_icon)
        val title: TextView = itemView.findViewById(R.id.card_title)
        icon.setImageResource(listCard[position].icon.image)
        title.text = listCard[position].title
        return itemView
    }
}