package com.example.labs.lab0

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.labs.databinding.ActivityLabZeroBinding

const val APP_PREFERENCES = "APP_PREFS"
const val PREFS_COUNTER_VALUE = "PREFS_COUNTER_VALUE"

class LabZeroActivity : AppCompatActivity() {
    private lateinit var binding: ActivityLabZeroBinding
    private lateinit var preferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLabZeroBinding.inflate(layoutInflater)
        setContentView(binding.root)
        preferences = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE)
        binding.textCounter.text = preferences.getInt(PREFS_COUNTER_VALUE, 0).toString()

        binding.btnMinus.setOnClickListener {
            var value = binding.textCounter.text.toString().toInt()
            value--
            preferences.edit()
                .putInt(PREFS_COUNTER_VALUE, value)
                .apply()
            binding.textCounter.text = preferences.getInt(PREFS_COUNTER_VALUE, 0).toString()
        }

        binding.btnPlus.setOnClickListener {
            var value = binding.textCounter.text.toString().toInt()
            value++
            preferences.edit()
                .putInt(PREFS_COUNTER_VALUE, value)
                .apply()
            binding.textCounter.text = preferences.getInt(PREFS_COUNTER_VALUE, 0).toString()
        }
    }
}