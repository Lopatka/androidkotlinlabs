package com.example.labs.lab3.network

import android.util.Log
import io.ktor.client.*
import io.ktor.client.engine.android.*
import io.ktor.client.features.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.features.logging.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import kotlinx.serialization.json.Json

const val URL: String = "http://192.168.0.2:9090"

object KtorClient {
    val json = Json {
        encodeDefaults = true
        ignoreUnknownKeys = true
        isLenient = true
    }

    val httpClient = HttpClient(Android) {
        expectSuccess = true
        HttpResponseValidator {
            validateResponse { response: HttpResponse ->
                when (response.status.value) {
                    in 300..399 -> throw RedirectResponseException(response, "redirect")
                    in 400..499 -> throw ClientRequestException(response, "client error")
                    in 500..599 -> throw ServerResponseException(response, "server error")
                }
            }
            handleResponseException { cause: Throwable ->
                throw cause
            }
        }

        install(HttpTimeout) {
            socketTimeoutMillis = 30000
            requestTimeoutMillis = 30000
            connectTimeoutMillis = 30000
        }

        install(Logging){
            logger = object: Logger{
                override fun log(message: String) {
                    Log.d("NETWORK", "log: $message")
                }
            }
        }

        install(JsonFeature){
            serializer = KotlinxSerializer(json)
        }

        defaultRequest {
            contentType(ContentType.Application.Json)
            accept(ContentType.Application.Json)
        }
    }
}