package com.example.labs.lab3

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import com.example.labs.R
import com.example.labs.databinding.ActivityLabThirdBinding
import com.example.labs.lab0.LabZeroActivity
import com.example.labs.lab1.DetailScreen
import com.example.labs.lab1.LabFirstActivity
import com.example.labs.lab1.model.card.Card
import com.example.labs.lab2.LabSecondActivity
import com.example.labs.lab3.model.User

class LabThirdActivity : AppCompatActivity() {
    private lateinit var binding: ActivityLabThirdBinding
    private lateinit var toggle: ActionBarDrawerToggle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLabThirdBinding.inflate(layoutInflater)
        setContentView(binding.root)
        toggle = ActionBarDrawerToggle(this, binding.drawerLayout, R.string.open, R.string.close)
        binding.drawerLayout.addDrawerListener(toggle)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
        toggle.syncState()
        setNavigationOptions()

        val user = intent.getSerializableExtra("User") as User
        binding.textView.text = user.toString()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (toggle.onOptionsItemSelected(item)) {
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setNavigationOptions(){
        binding.apply {
            navView.setNavigationItemSelectedListener {
                when(it.itemId){
                    R.id.nav_products ->{
                        val intent = Intent(this@LabThirdActivity, ProductActivity::class.java)
                        startActivity(intent)
                    }
                    R.id.nav_orders ->{
                        val intent = Intent(this@LabThirdActivity, OrderActivity::class.java)
                        startActivity(intent)
                    }
                }
                true
            }
        }
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)){
            binding.drawerLayout.closeDrawers()
        }
    }
}