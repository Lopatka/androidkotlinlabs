package com.example.labs.lab1.counter

class CollatzCounter(private var value: Int) {

    fun setValue(value: Int) {
        this.value = value
    }

    fun getValue(): Int {
        return value
    }

    fun next(): Int {
        if (value % 2 == 0) {
            value /= 2
        } else {
            value = value * 3 + 1
        }
        return value
    }
}