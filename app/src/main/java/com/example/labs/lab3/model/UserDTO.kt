package com.example.labs.lab3.model

import kotlinx.serialization.Serializable

@Serializable
data class UserDTO(
    val id: Long,
    val login: String,
    val password: String,
    val role: Long
)
