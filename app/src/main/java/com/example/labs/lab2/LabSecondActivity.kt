package com.example.labs.lab2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.labs.R
import com.example.labs.databinding.ActivityLabSecondBinding
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.locks.Condition
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock

const val START_VALUE: Long = 0
const val STEP_SPEED_FIRST: Long = 30
const val STEP_SPEED_SECOND: Long = 20
const val MAX_SPEED: Long = 20
const val MIN_SPEED: Long = 1
const val FIRST: Int = 1
const val SECOND: Int = 2

class LabSecondActivity : AppCompatActivity() {
    private lateinit var binding: ActivityLabSecondBinding

    @Volatile
    var isStopped = AtomicBoolean(false)
    private set
    private var delayFirst: Long = 600
    private var delaySecond: Long = 400
    private var isClickedOnStop = false

    @Volatile
    var speedFirst: Long = MIN_SPEED
        private set

    @Volatile
    var speedSecond: Long = MIN_SPEED
        private set

    private lateinit var threadFirst: Thread
    private lateinit var threadSecond: Thread
    private lateinit var customRunnableFirst: CustomRunnable
    private lateinit var customRunnableSecond: CustomRunnable

    var reentrantLock = ReentrantLock()

    var condition: Condition = reentrantLock.newCondition()
        private set

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLabSecondBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setTitle(R.string.task2_name)
        binding.textViewThreadFirst.text = START_VALUE.toString()
        binding.textViewThreadSecond.text = START_VALUE.toString()
        binding.textViewThreadsSpeedFirst.text = speedFirst.toString()
        binding.textViewThreadsSpeedSecond.text = speedSecond.toString()
        customRunnableFirst = CustomRunnable(delayFirst, binding.textViewThreadFirst, this, FIRST)
        threadFirst = Thread(customRunnableFirst)
        customRunnableSecond = CustomRunnable(delaySecond, binding.textViewThreadSecond, this, SECOND)
        threadSecond = Thread(customRunnableSecond)
    }

    override fun onResume() {
        super.onResume()
        if ((!isClickedOnStop) &&
            (threadFirst.state.equals(Thread.State.WAITING)) &&
            (threadSecond.state.equals(Thread.State.WAITING))) {
            isStopped.set(false)
            reentrantLock.withLock {
                condition.signalAll()
            }
        }
    }

    override fun onStop() {
        isStopped.set(true)
        super.onStop()
    }

    override fun onDestroy() {
        threadFirst.interrupt()
        threadSecond.interrupt()
        super.onDestroy()
    }

    fun onClickRun(view: View) {
        isClickedOnStop = false
        isStopped.set(false)
        if (threadFirst.state.equals(Thread.State.NEW)) {
            threadFirst.start()
            threadSecond.start()
        } else {
            reentrantLock.withLock {
                condition.signalAll()
            }
        }
    }

    fun onClickStop(view: View) {
        isClickedOnStop = true
        isStopped.set(true)
    }

    fun onClickReset(view: View) {
        isClickedOnStop = true
        isStopped.set(true)
        binding.textViewThreadFirst.text = customRunnableFirst.reset().toString()
        binding.textViewThreadSecond.text = customRunnableSecond.reset().toString()
    }

    fun onClickUpSpeedFirst(view: View) {
        if (speedFirst < MAX_SPEED) {
            speedFirst++
            binding.textViewThreadsSpeedFirst.text = speedFirst.toString()
        }
    }

    fun onClickDownSpeedFirst(view: View) {
        if (speedFirst > MIN_SPEED) {
            speedFirst--
            binding.textViewThreadsSpeedFirst.text = speedFirst.toString()
        }
    }

    fun onClickUpSpeedSecond(view: View) {
        if (speedSecond < MAX_SPEED) {
            speedSecond++
            binding.textViewThreadsSpeedSecond.text = speedSecond.toString()
        }
    }

    fun onClickDownSpeedSecond(view: View) {
        if (speedSecond > MIN_SPEED) {
            speedSecond--
            binding.textViewThreadsSpeedSecond.text = speedSecond.toString()
        }
    }
}

