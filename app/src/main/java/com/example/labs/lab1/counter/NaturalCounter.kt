package com.example.labs.lab1.counter

class NaturalCounter(private var value: Int) {
    fun setValue(value: Int) {
        this.value = value
    }

    fun getValue(): Int {
        return value
    }

    fun next(): Int {
        return ++value
    }
}