package com.example.labs.lab1.model.card

import com.example.labs.R


enum class Icon(val image: Int) {
    DOG(R.drawable.dog),
    CAT(R.drawable.cat),
    FISH(R.drawable.fish),
    RABBIT(R.drawable.rabbit);
}