package com.example.labs.lab3

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.labs.R
import com.example.labs.databinding.ActivityAuthBinding
import com.example.labs.lab3.model.LoginDTO
import com.example.labs.lab3.model.User
import com.example.labs.lab3.model.UserDTO
import com.example.labs.lab3.repository.UserRepository
import io.ktor.client.features.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class AuthActivity : AppCompatActivity() {
    private lateinit var binding: ActivityAuthBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAuthBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    @SuppressLint("SetTextI18n")
    fun onClickSignIn(view: View) {
        var user : User?
        CoroutineScope(Dispatchers.IO).launch {
            val loginDTO = LoginDTO(
                login = binding.editTextTextPersonLogin.text.toString(),
                password = binding.editTextTextPassword.text.toString()
            )
            try {
                user = UserRepository().postAuth(loginDTO)
                runOnUiThread {
                    val intent = Intent(this@AuthActivity, LabThirdActivity::class.java)
                    intent.putExtra("User", user)
                    startActivity(intent)
                }
            } catch (ex: ClientRequestException) {
                binding.textViewMessage.setTextColor(resources.getColor(R.color.red))
                binding.textViewMessage.text = "Incorrect login or password"
            }
        }
    }

    @SuppressLint("SetTextI18n")
    fun onClickSignUp(view: View) {
        val myPrefs = getSharedPreferences(APP_LAB3_PREFERENCES, Context.MODE_PRIVATE)
        val editor = myPrefs.edit()
        editor.putString(PREFS_CART_JSON, null)
        editor.apply()

        CoroutineScope(Dispatchers.IO).launch {
            val userDTO = UserDTO(
                id = 0,
                login = binding.editTextTextPersonLogin.text.toString(),
                password = binding.editTextTextPassword.text.toString(),
                role = 1
            )
            try {
                UserRepository().postAddUser(userDTO)
                runOnUiThread {
                    binding.textViewMessage.setTextColor(resources.getColor(R.color.green))
                    binding.textViewMessage.text = "User is registered"
                }
            } catch (ex: ClientRequestException) {

            }
        }
    }
}