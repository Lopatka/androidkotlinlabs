package com.example.labs.lab3.repository

import com.example.labs.lab3.model.product.Product
import com.example.labs.lab3.network.KtorClient
import com.example.labs.lab3.network.URL
import io.ktor.client.request.*


class ProductRepository {
    suspend fun getProducts(): List<Product> =
        KtorClient.httpClient.get {
            url("$URL/products/all")
        }
}