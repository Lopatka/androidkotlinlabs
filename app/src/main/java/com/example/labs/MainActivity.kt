package com.example.labs

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import com.example.labs.databinding.ActivityMainBinding
import com.example.labs.lab0.LabZeroActivity

import com.example.labs.lab1.LabFirstActivity
import com.example.labs.lab2.LabSecondActivity
import com.example.labs.lab3.AuthActivity
import com.example.labs.lab3.LabThirdActivity


class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var toggle: ActionBarDrawerToggle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setTitle(R.string.app_name)

        toggle = ActionBarDrawerToggle(this, binding.drawerLayout, R.string.open, R.string.close)
        binding.drawerLayout.addDrawerListener(toggle)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
        toggle.syncState()
        setNavigationOptions()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (toggle.onOptionsItemSelected(item)) {
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setNavigationOptions(){
        binding.apply {
            navView.setNavigationItemSelectedListener {
                when(it.itemId){
                    R.id.nav_task0 ->{
                        val intent = Intent(this@MainActivity, LabZeroActivity::class.java)
                        startActivity(intent)
                    }
                    R.id.nav_task1 ->{
                        val intent = Intent(this@MainActivity, LabFirstActivity::class.java)
                        startActivity(intent)
                    }
                    R.id.nav_task2 -> {
                        val intent = Intent(this@MainActivity, LabSecondActivity::class.java)
                        startActivity(intent)
                    }
                    R.id.nav_task3 -> {
                        val intent = Intent(this@MainActivity, AuthActivity::class.java)
                        startActivity(intent)
                    }
                }
                true
            }
        }
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)){
            binding.drawerLayout.closeDrawers()
        }
    }
}