package com.example.labs.lab3.model.product

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.example.labs.R
import com.example.labs.lab3.PREFS_CART_JSON
import com.example.labs.lab3.ProductActivity
import com.google.gson.Gson


class ProductAdapter(
    private val getContext: Context,
    private val layoutId: Int,
    private val listProducts: List<Product>
) : ArrayAdapter<Product>(getContext, layoutId, listProducts) {

    @SuppressLint("ViewHolder", "SetTextI18n", "UseCompatLoadingForColorStateLists")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val inflater = (getContext as Activity).layoutInflater
        val itemView: View = inflater.inflate(layoutId, parent, false)
        val title: TextView = itemView.findViewById(R.id.product_card_title)
        val priceBtn: Button = itemView.findViewById(R.id.product_card_price)
        val cart: TextView = (getContext as ProductActivity).findViewById(R.id.text_view_products_count)

        title.text = listProducts[position].title
        priceBtn.text = listProducts[position].price.toString() + "₽"

        if (listProducts[position].isDeleted) {
            priceBtn.backgroundTintList = getContext.resources.getColorStateList(R.color.gray)
            priceBtn.setOnClickListener {
                Toast.makeText(getContext, "Товара нет в наличии", Toast.LENGTH_LONG).show()
            }
        } else {
            priceBtn.setOnClickListener {
                getContext.products.add(listProducts[position])
                val gson = Gson()
                val json = gson.toJson(getContext.products)
                getContext.preferences.edit()
                    .putString(PREFS_CART_JSON, json)
                    .apply()
                cart.text = "Товаров в корзине: " +getContext.products.size.toString()
                Toast.makeText(
                    getContext,
                    listProducts[position].title + " была добавлена в корзину",
                    Toast.LENGTH_LONG
                ).show()
            }
        }
        return itemView
    }
}