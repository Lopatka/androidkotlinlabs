package com.example.labs.lab2

import android.widget.TextView
import kotlin.concurrent.withLock

class CustomRunnable(
    private var delay: Long,
    private var textView: TextView,
    private val activity: LabSecondActivity,
    private var id: Int
) : Runnable {
    private var count = 0

    fun reset(): Int {
        count = 0
        return count
    }

    override fun run() {
        while (count < Int.MAX_VALUE) {
            activity.reentrantLock.withLock {
                while (activity.isStopped.get()) {
                    try {
                        activity.condition.await()
                    } catch (e: InterruptedException) {
                        return
                    }
                }
            }
            count++
            activity.runOnUiThread {
                textView.text = count.toString()
            }
            if (id == FIRST) {
                Thread.sleep((delay + activity.speedFirst * -STEP_SPEED_FIRST).coerceAtLeast(STEP_SPEED_FIRST))
            } else if (id == SECOND) {
                Thread.sleep((delay + activity.speedSecond * -STEP_SPEED_SECOND).coerceAtLeast(STEP_SPEED_SECOND))
            }
        }
    }
}