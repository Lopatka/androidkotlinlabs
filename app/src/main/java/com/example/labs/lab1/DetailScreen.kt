package com.example.labs.lab1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.labs.R
import com.example.labs.databinding.ActivityDetailScreenBinding
import com.example.labs.lab1.counter.CollatzCounter
import com.example.labs.lab1.counter.FibonacciCounter
import com.example.labs.lab1.counter.NaturalCounter
import com.example.labs.lab1.model.card.Card

const val NATURAL_NUMBER_KEY = "NATURAL_NUMBER_KEY"
const val FIBONACCI_KEY = "FIBONACCI_KEY"
const val COLLATZ_KEY = "COLLATZ_KEY"

class DetailScreen : AppCompatActivity() {
    companion object {
        const val CARD = "CARD"
    }

    private lateinit var binding: ActivityDetailScreenBinding

    private var naturalCounter: NaturalCounter = NaturalCounter(1)
    private var fibonacciCounter: FibonacciCounter = FibonacciCounter()
    private var collatzCounter: CollatzCounter = CollatzCounter(5)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setTitle(R.string.detail_screen_name)

        val card = intent.getSerializableExtra(CARD) as Card
        binding.textViewDescription.text = card.description
        binding.imageViewIcon.setImageResource(card.icon.image)
        binding.textViewTitle.text = card.title

        binding.textViewNatural.text = naturalCounter.getValue().toString()
        binding.textViewFibonacci.text = fibonacciCounter.getPrev().toString()
        binding.textViewCollatz.text = collatzCounter.getValue().toString()
    }

    fun onClickNextNatural(view: View) {
        binding.textViewNatural.text = naturalCounter.next().toString()
    }

    fun onClickNextFibonacci(view: View) {
        binding.textViewFibonacci.text = fibonacciCounter.next().toString()
    }

    fun onClickNextCollatz(view: View) {
        binding.textViewCollatz.text = collatzCounter.next().toString()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putInt(NATURAL_NUMBER_KEY, naturalCounter.getValue())
        outState.putIntArray(FIBONACCI_KEY, intArrayOf(fibonacciCounter.getPrev(),fibonacciCounter.getCurr()))
        outState.putInt(COLLATZ_KEY, collatzCounter.getValue())
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        naturalCounter.setValue(savedInstanceState.getInt(NATURAL_NUMBER_KEY))
        savedInstanceState.getIntArray(FIBONACCI_KEY)?.get(0)?.let { fibonacciCounter.setPrev(it) }
        savedInstanceState.getIntArray(FIBONACCI_KEY)?.get(1)?.let { fibonacciCounter.setCurr(it) }
        collatzCounter.setValue(savedInstanceState.getInt(COLLATZ_KEY))

        binding.textViewNatural.text = naturalCounter.getValue().toString()
        binding.textViewFibonacci.text = fibonacciCounter.getPrev().toString()
        binding.textViewCollatz.text = collatzCounter.getValue().toString()
    }
}