package com.example.labs.lab3.model.product

import com.example.labs.lab3.helpers.BigDecimalSerializer
import kotlinx.serialization.Serializable
import java.math.BigDecimal

@Serializable
data class Product(
    val id: Long,
    val title: String,
    @Serializable(with = BigDecimalSerializer::class)
    val price: BigDecimal,
    var isDeleted: Boolean
) : java.io.Serializable
{
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        other as Product
        return id == other.id
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + title.hashCode()
        result = 31 * result + price.hashCode()
        result = 31 * result + isDeleted.hashCode()
        return result
    }
}
