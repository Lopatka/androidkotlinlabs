package com.example.labs.lab1

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.example.labs.R
import com.example.labs.databinding.ActivityLabFirstBinding

import com.example.labs.lab1.model.card.Card
import com.example.labs.lab1.model.card.DataSourceCard
import com.google.android.material.snackbar.Snackbar

const val APP_PREFERENCES = "APP_PREFERENCES"
const val PREFS_COLOR_VALUE = "PREFS_COLOR_VALUE"
const val PREFS_LABEL_VALUE = "PREFS_LABEL_VALUE"
const val PREFS_SWITCH_CHECKED = "PREFS_SWITCH_CHECKED"
const val TASK_LOG = "TestTask1"

class LabFirstActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLabFirstBinding
    private lateinit var preferences: SharedPreferences
    private var dataSourceCard: DataSourceCard = DataSourceCard()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.Theme_Task1)
        binding = ActivityLabFirstBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setTitle(R.string.task1_name)
        preferences = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE)
        loadPreferences()

        //access the listView from xml file
        val arrayAdapter: ArrayAdapter<*> = CardAdapter(
            this,
            R.layout.card,
            dataSourceCard.fetchData()
        )
        binding.listView.adapter = arrayAdapter

        binding.listView.setOnItemClickListener { parent, _, position, _ ->
            val element = parent.getItemAtPosition(position) as Card
            val intent = Intent(this, DetailScreen::class.java)
            intent.putExtra(DetailScreen.CARD, element)
            startActivity(intent)
        }
    }

    private fun loadPreferences() {
        binding.textLabel.setTextColor(
            preferences.getInt(PREFS_COLOR_VALUE, ContextCompat.getColor(this, R.color.green))
        )
        binding.textLabel.text = preferences.getString(PREFS_LABEL_VALUE, "Label")
        binding.switchColor.isChecked = preferences.getBoolean(PREFS_SWITCH_CHECKED, false)
    }

    fun onClickChangeColor(view: View) {
        if (binding.textLabel.currentTextColor == ContextCompat.getColor(this, R.color.green)) {
            binding.textLabel.setTextColor(ContextCompat.getColor(this, R.color.red))
        } else {
            binding.textLabel.setTextColor(ContextCompat.getColor(this, R.color.green))
        }
        preferences.edit()
            .putBoolean(PREFS_SWITCH_CHECKED,binding.switchColor.isChecked)
            .putInt(PREFS_COLOR_VALUE, binding.textLabel.currentTextColor)
            .apply()
    }

    fun onClickShowToast(view: View) {
        Toast.makeText(this, "show toast", Toast.LENGTH_SHORT).show()
        Log.d(TASK_LOG, "onClickShowToast")
    }

    fun onClickChangeLabelText(view: View) {
        binding.textLabel.text = binding.editText.text
        preferences.edit()
            .putString(PREFS_LABEL_VALUE, binding.textLabel.text.toString())
            .apply()
        Snackbar.make(binding.coordinatorLayout, "Text changed", Snackbar.LENGTH_SHORT).show()
    }

    fun onClickHideList(view: View) {
        if (binding.listView.visibility == View.VISIBLE) {
            binding.listView.visibility = View.INVISIBLE
        } else {
            binding.listView.visibility = View.VISIBLE
        }
        Log.d(TASK_LOG, "onClickHideList")
    }
}