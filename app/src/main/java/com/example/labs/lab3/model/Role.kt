package com.example.labs.lab3.model

import kotlinx.serialization.Serializable

@Serializable
data class Role(
    val id: Long,
    val name: UserRole
): java.io.Serializable
