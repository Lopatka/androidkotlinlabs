package com.example.labs.lab1.model.card

import java.io.Serializable


data class Card (
    val title: String,
    val description: String,
    val icon: Icon
): Serializable