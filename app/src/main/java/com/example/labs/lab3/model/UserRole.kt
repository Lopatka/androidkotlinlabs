package com.example.labs.lab3.model

enum class UserRole(private val roleName: String) {
    ADMIN("ROLE_ADMIN"),
    USER("ROLE_USER"),
}