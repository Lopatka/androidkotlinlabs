package com.example.labs.lab1.counter

class FibonacciCounter {
    private var prev: Int = 0
    private var curr: Int = 1

    fun getPrev(): Int {
        return prev
    }

    fun getCurr(): Int {
        return curr
    }

    fun setPrev(value: Int){
        prev = value
    }

    fun setCurr(value: Int){
        curr = value
    }

    fun next(): Int {
        val temp = curr
        curr += prev
        prev = temp
        return prev
    }
}