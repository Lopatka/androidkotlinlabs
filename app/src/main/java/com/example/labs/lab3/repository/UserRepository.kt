package com.example.labs.lab3.repository

import com.example.labs.lab3.model.LoginDTO
import com.example.labs.lab3.model.User
import com.example.labs.lab3.model.UserDTO
import com.example.labs.lab3.network.KtorClient
import com.example.labs.lab3.network.URL
import io.ktor.client.request.*



class UserRepository {

    suspend fun postAuth(loginDTO: LoginDTO): User = KtorClient.httpClient.post {
        url("$URL/users/auth")
        body = loginDTO
    }

    suspend fun postAddUser(userDto: UserDTO): User = KtorClient.httpClient.post {
        url("$URL/users/add")
        body = userDto
    }
}