package com.example.labs.lab3.model

import kotlinx.serialization.Serializable


@Serializable
data class User(
    val id: Long,
    val login: String,
    var isDeleted: Boolean,
    val role: Role
) : java.io.Serializable
