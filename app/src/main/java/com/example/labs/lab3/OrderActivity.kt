package com.example.labs.lab3

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.example.labs.databinding.ActivityOrderBinding

class OrderActivity : AppCompatActivity() {
    private lateinit var binding: ActivityOrderBinding
    lateinit var preferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOrderBinding.inflate(layoutInflater)
        setContentView(binding.root)
        preferences = getSharedPreferences(APP_LAB3_PREFERENCES, Context.MODE_PRIVATE)
    }

    fun onClickClearOrder(view: View) {
        preferences.edit()
            .putString(PREFS_CART_JSON, null)
            .apply()
        Toast.makeText(this, "Корзина очищена", Toast.LENGTH_LONG).show()
    }

    fun onClickGoToProducts(view: View) {
        val intent = Intent(this, ProductActivity::class.java)
        startActivity(intent)
    }
}