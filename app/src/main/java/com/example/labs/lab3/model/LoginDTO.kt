package com.example.labs.lab3.model

import kotlinx.serialization.Serializable

@Serializable
data class LoginDTO(
    val login: String,
    val password: String
)
